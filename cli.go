package main

import (
	"context"
	pb "csmt-cli/proto/consignment"
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"os"

	micro "github.com/micro/go-micro"
)

const (
	FILE = "consignment.json"
)

func parseFile(filename string) (*pb.Consignment, error) {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	var consignment *pb.Consignment
	err = json.Unmarshal(data, &consignment)
	if err != nil {
		return nil, errors.New("consignment.json file content error" + err.Error())
	}

	return consignment, nil
}

func main() {
	service := micro.NewService(micro.Name("consignment.client"))
	service.Init()
	client := pb.NewShippingService("consignment", service.Client())

	datafile := FILE

	if len(os.Args) > 1 {
		datafile = os.Args[1]
	}

	consignment, err := parseFile(datafile)
	if err != nil {
		log.Fatalf("parse into file error: %v", err)
	}

	rsp, err := client.Hello(context.Background(), &pb.HelloRequest{Name: "vritser"})
	if err != nil {
		log.Fatal(err)
	}
	log.Println(rsp.Greeting)
	resp, err := client.CreateConsignment(context.TODO(), consignment)
	if err != nil {
		log.Fatalf("create consignment error: %v", err)
	}

	log.Printf("created: %t", resp.Created, resp.Consignment)
}
