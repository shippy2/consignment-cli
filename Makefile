build:
	protoc -I proto proto/consignment/consignment.proto --micro_out=proto --go_out=proto

docker:
	docker build -t consignment-cli .

run:
	docker run -e MICRO_REGISTRY=mdns consignment-cli
